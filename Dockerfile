#FROM openkm/openkm-ce:6.3.8
FROM openkm/openkm-ce:6.3.8
#VOLUMEN 6.3.8
#/opt/openkm/

#FROM openkm/openkm-ce
#####
#VOLUMEN 6.3.9
#/opt/tomcat
#FROM openkm/openkm-ce:6.3.9
#FROM openkm/openkm-ce:6.3.9


WORKDIR /
#ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_FRONTEND noninteractive


ENV TZ=America/El_Salvador
RUN apt-get update 
RUN apt-get install -y tzdata 
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata




# Update the repository sources list
RUN apt-get update

COPY ./policy.xml /etc/ImageMagick-6/policy.xml
#COPY ./OpenKM.cfg /opt/openkm/OpenKM.cfg
#COPY ./OpenKM.xml /opt/openkm/OpenKM.xml
#COPY ./server.xml /opt/openkm/conf/OpenKM.cfg 

#COPY ./policy.xml /etc/ImageMagick-6/policy.xml
#COPY ./OpenKM.cfg /opt/tomcat/OpenKM.cfg
#COPY ./OpenKM.xml /opt/tomcat/OpenKM.xml
#COPY ./server.xml /opt/tomcat/conf/OpenKM.cfg 


#VOLUMEN
#/opt/tomcat
#/opt/openkm/


RUN apt-get install -y nano build-essential imagemagick --fix-missing




